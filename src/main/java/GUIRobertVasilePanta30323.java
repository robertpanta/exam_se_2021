import javax.swing.*;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class GUIRobertVasilePanta30323 extends JFrame {
    JLabel fileNameLabel, textLabel;
    JButton writeButton;
    JTextField fileNameTextField, textField;
    public static final int width = 600, height = 180;

    public static void main(String[] args) {
        GUIRobertVasilePanta30323 robertVasilePanta30323 = new GUIRobertVasilePanta30323();
    }

    public GUIRobertVasilePanta30323() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLayout(null);
        this.setSize(width, height);
        createApplicationElements();

        this.setVisible(true);
    }

    private void createApplicationElements() {

        fileNameLabel = new JLabel();
        fileNameLabel.setText("File name");
        fileNameLabel.setBounds(10, 10, 100, 30);

        textLabel = new JLabel();
        textLabel.setText("Text");
        textLabel.setBounds(10, 50, 100, 30);

        writeButton = new JButton();
        writeButton.setText("Write!");
        writeButton.setBounds(10, 90, 100, 30);

        fileNameTextField = new JTextField();
        fileNameTextField.setBounds(130, 10, 100, 30);

        textField = new JTextField();
        textField.setBounds(130, 50, 400, 30);

        writeButton.addActionListener(e -> writeText());

        this.add(fileNameLabel);
        this.add(fileNameTextField);
        this.add(textLabel);
        this.add(textField);
        this.add(writeButton);
    }

    private void writeText() {
        String fileName = fileNameTextField.getText();
        if (fileName.equals("")) {
            fileName = "theFileDoesn_tHasAName.txt"; //in case there isn't any file name given
        }
        try {
            File myFile = new File(fileName);
            if (myFile.createNewFile()) {
                System.out.println("File succesfully created!!!");
            } else {
                System.out.println("File already exists! It will be overwritten!");
            }
            FileWriter fileWriter = new FileWriter(fileName);
            fileWriter.write(textField.getText());
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        fileNameTextField.setText("");
        textField.setText("");
        //after pressing the button, the Textfields will be empty, getting ready for another writing in a file
    }
}