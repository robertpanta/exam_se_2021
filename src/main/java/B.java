public class B extends A {

    private long t;
    private D d; // adding a parameter for class D

    public void b() {
        //code
    }

    C c;

    public void usingD(D d) {
        //code using d
    }
}

class A {

}

class C {

}

class D implements Y {
    private E e; // adding a parameter for class E
    private F f; // adding a parameter for class F

    public void met1(int i) {
        //code
    }

    public D() {
        e = new E();
    }

    public void setF(F f) {
        this.f = f;
    }

}

class E {

    public void met2() {
        //code
    }

}

class F {

}

interface Y {

}